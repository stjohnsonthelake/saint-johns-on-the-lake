Saint John's On The Lake is an exceptional retirement community overlooking beautiful Lake Michigan in Milwaukee. With grand, maintenance free apartment homes and daily encouragement of spiritual, cultural and educational growth, retirement at Saint John's has limitless possibilities.

Address: 1840 North Prospect Avenue, Milwaukee, WI 53202, USA

Phone: 414-272-2022

Website: https://www.saintjohnsmilw.org
